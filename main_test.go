package main

import (
	"testing"
)

func TestAdd(t *testing.T) {
	exp := "-addstore Root some-path"
	act := create_add_command("Root", "some-path")

	if exp != act {
		t.Fatalf("Expected result not matches with actual\nExp: %s : Act: %s", exp, act)
	}
}

func TestDelete(t *testing.T) {
	exp := "-delstore Root ab098d2cfe242e0c"
	act, err := create_delete_command("Root", "./Inside.der")
	if err != nil {
		t.Fatalf("Failed: %+v", err)
	}

	if exp != act {
		t.Fatalf("Expected: %s, Given: %s", exp, act)
	}
}

func TestReadSerialNumber(t *testing.T) {
	exp := "ab098d2cfe242e0c"
	cert, err := read_cert_from_file("./Inside.der")
	if err != nil {
		t.Fatalf("Failed: %+v", err)
	}

	act := get_serial_number_as_hex(cert)

	if exp != act {
		t.Fatalf("Expected: %s, Given: %s", exp, act)
	}
}

func TestReadUnknownFileAsCert(t *testing.T) {
	_, err := read_cert_from_file("./Non-Existing-File")

	if err == nil {
		t.Fatalf("Expected func to fail")
	}
}

func TestGetSerialNumberFromPEM(t *testing.T) {
	exp := "ab098d2cfe242e0c"
	cert, err := parse_cert_from_pem()
	if err != nil {
		t.Fatalf("Failed: %+v", err)
	}

	act := cert.SerialNumber.Text(16)

	if exp != act {
		t.Fatalf("Expected: %s, Given: %s", exp, act)
	}
}
