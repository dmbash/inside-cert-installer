package main

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"log"
	"os"
	"os/exec"
	"strings"
)

const (
	util              = "certutil.exe"
	add_to_store      = "-addstore"
	delete_from_store = "-delstore"
)

const certPEM string = `
-----BEGIN CERTIFICATE-----
MIICYjCCAcugAwIBAgIJAKsJjSz+JC4MMA0GCSqGSIb3DQEBBQUAMEoxCzAJBgNV
BAYTAlJVMQwwCgYDVQQIDANOU08xFDASBgNVBAcMC05vdm9zaWJpcnNrMRcwFQYD
VQQKDA5JbnNpZGUgU3lzdGVtczAeFw0xNDAzMjAwOTQ2MDNaFw00MTA4MDQwOTQ2
MDNaMEoxCzAJBgNVBAYTAlJVMQwwCgYDVQQIDANOU08xFDASBgNVBAcMC05vdm9z
aWJpcnNrMRcwFQYDVQQKDA5JbnNpZGUgU3lzdGVtczCBnzANBgkqhkiG9w0BAQEF
AAOBjQAwgYkCgYEA1CTZnHsJoCNw2ydiF374fNIEGx6DKGC3+tzxQdyWOgSt/ku5
MWaVnuK7/hTJNROBIihcgMshwL9Fh74kR4ZzQjdwcsG50hQCmFuGtZBROoJdMRwd
nqonMFyaDrn3ayUKPyL8LWsMxsm0O+D95meqPU9yoQL8jGHIAa9YZ+RzMyMCAwEA
AaNQME4wHQYDVR0OBBYEFPzwSN/yQn0QFCkd5/dnCmFzCyzdMB8GA1UdIwQYMBaA
FPzwSN/yQn0QFCkd5/dnCmFzCyzdMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEF
BQADgYEAukhUuts77j62Hc7BJqvwU3HwDtkYpnrdFwwexgtfABNaf3gd8LYT7S8f
laLhM87URe0NW7X8aTZd5py8NDyq1S2XdmWlTI2VDMCu1eqV8JJ3yZAXcckXuf4F
InU2oQa6jmOShgROJYY5chNCi+CDJPS2ghIoRpG1CR0I5uI0s7s=
-----END CERTIFICATE-----
`

// default behaviour: install certificate in Root store
func main() {
	// parse command line argumentsp
	temp_file_cert, err := write_cert_from_pem()
	if err != nil {
		log.Fatalf("%+v", err)
	}

	file_name := temp_file_cert.Name()
	temp_file_cert.Close()

	// TODO: Need to elevate privileges!
	// 0x800702e4 (WIN32: 740 ERROR_ELEVATION_REQUIRED) -- 2147943140 (-2147024156)
	cmd := exec.Command("certutil.exe", "-addstore", "Root", file_name)
	stdout, err := cmd.Output()
	os.Remove(temp_file_cert.Name())
	if err != nil {
		log.Fatalf("%s", err.Error())
	}
	log.Printf(string(stdout))
}

func parse_cert_from_pem() (*x509.Certificate, error) {
	block, _ := pem.Decode([]byte(certPEM))
	log.Printf("block type is: %s", block.Type)
	if block == nil {
		return nil, errors.New("Failed to decode PEM while reading")
	}

	cert, err := x509.ParseCertificate(block.Bytes)

	if err != nil {
		return nil, err
	}

	return cert, nil
}

func write_cert_from_pem() (*os.File, error) {
	block, _ := pem.Decode([]byte(certPEM))

	if block == nil {
		return nil, errors.New("Failed to decode PEM while writing")
	}
	pemCertificate, err := os.CreateTemp(".", "tmpfile-*.cer")
	if err != nil {
		return nil, err
	}

	err = pem.Encode(pemCertificate, block)
	if err != nil {
		return nil, err
	}
	return pemCertificate, nil
}

func read_cert_from_file(cert_path string) (*x509.Certificate, error) {
	cert_file, err := os.Open(cert_path)
	if err != nil {
		return nil, err
	}
	defer cert_file.Close()

	derBytes := make([]byte, 1000)

	count, err := cert_file.Read(derBytes)
	if err != nil {
		return nil, err
	}

	cert, err := x509.ParseCertificate(derBytes[0:count])
	if err != nil {
		return nil, err
	}

	return cert, nil
}

func get_serial_number_as_hex(cert *x509.Certificate) string {
	return cert.SerialNumber.Text(16)
}

// create_add_command - generates shell command to add certificate on given cert-path to cryptographic store
func create_add_command(store string, cert_path string) string {
	return strings.Join([]string{add_to_store, store, cert_path}, " ")
}

func create_delete_command(store string, cert_path string) (string, error) {
	cert, err := read_cert_from_file(cert_path)
	if err != nil {
		return "", err
	}

	return strings.Join([]string{delete_from_store, store, get_serial_number_as_hex(cert)}, " "), nil
}
